<table class="table" style="height:100% !important">
    <tbody>
        <tr>
            <td>
                <div class="">
                    <div class="card-body">
                    <?php
                    
                    echo '<form method="post" action="options.php">';

                    do_settings_sections( 'wafvel-options-page' );
                    settings_fields( 'wafvel-settings' );
                
                    submit_button();
                
                    echo '</form>';

                    ?>
                    <button id="testbutton" class="button button-success">Test Notification to Admin</button>
                    </div>
                </div>
            </td>
            <td>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Admin Notification</h4>
                        <a href="<?php echo plugin_dir_url( dirname(__FILE__) )?>img/cust.jpg" target="_blank" rel="noopener noreferrer"><img src="<?php echo plugin_dir_url( dirname(__FILE__) )?>img/admin.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" style="width:200px" alt="admin-notif"></a>
                        <p class="card-text">This show notification send to admin phone.</p>
                    </div>
                </div>
            </td>
            
            <td>
                <div class="card style="height:100%"">
                    <div class="card-body">
                        <h4 class="card-title">Customer Notification</h4>
                        <a href="<?php echo plugin_dir_url( dirname(__FILE__) )?>img/cust.jpg" target="_blank" rel="noopener noreferrer"><img src="<?php echo plugin_dir_url( dirname(__FILE__) )?>img/cust.jpg" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" style="width:200px" alt="cust-notif"></a>
                        <p class="card-text">This show notification send to customer phone.<br>Direct Transfer / BACS Payment Setting included.</p>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
