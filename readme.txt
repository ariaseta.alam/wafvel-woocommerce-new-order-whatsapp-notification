=== Wafvel WooCommerce New Order Whatsapp Notification ===
Contributors: randhipp
Donate link: https://wafvel.com/
Tags: notification, woocommerce, auto notification, order notification, new order notification, order info, whatsapp notification, whatsapp admin
Requires at least: 5.5
Tested up to: 5.5
Stable tag: 1.0.0-alpha
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Wafvel WooCommerce New Order Whatsapp Notification will send a chat to admin and customer every new order created at WooCommerce shop.

== Description ==

Wafvel WooCommerce New Order Whatsapp Notification using 3rd party API on wafvel.com to send whatsapp to admin phone number and customer phone number.

### Step 1 : Input Wafvel Token

*** How to get Token ***

    1. Register at wafvel.com/register - it's Free!
    2. Add your whatsapp number on add whatsapp page
    3. Copy your token
    4. Scan QR (like you use whatsapp web) using your phone whatsapp here
    5. Paste to wafvel options page on wordpress

### Step 2 : Input Admin Phone

This admin phone means any phone number (with whatsapp active)
which you want to send admin notification message to.

== Frequently Asked Questions ==

### What is WAFVEL?

WAFVEL is a WhatsApp Notification Gateway, Multi Admin Chat, which functions to send WhatsApp messages via RestAPI, or via a dashboard panel.

### How Does WAFVEL Work?

You only need to log in to the https://wafvel.com dashboard to be able to set up your whatsapp account, synchronize contacts, view your inbox and outbox, send messages, scan QR or view KEY / TOKEN APIs.

For developers, you can use WAFVEL for any purpose you like through our API at wafvel.com. If you already have a paid account at wafvel.com, the RestAPI feature is not subject to additional fees.
Whose number did the message send?

WAFVEL will use your own number.

### Can I still log in on my own computer's web.whatsapp?

WAFVEL automates your web.whatsapp on our server, so if you have scanned the whatsapp qr code on the WAFVEL server, you cannot use it simultaneously on your personal computer web.whatsapp, but you can use our dashboard to send, reply, or read messages that are come in.

### Is this service official from the WhatsApp (Facebook) company?

No, we are a self-service provider that automates the normal WA application to send messages without going through an event / generated system, such as notifications without having to be manually typed by your store Owner / CS.

### Can I Broadcast?

Not ! This service is not intended to SPAM / send random messages that the recipient does not want (Unsolicited Message).

### If I Use a Personal Number, Can My WA account Be Blocked (Banned)?

Yes, sending WA messages to people who have not saved your contacts, then the risk still exists if a large number of customers receive your message either intentionally / accidentally clicking 'spam' or 'report' in your message. send. Although the chances are very slim we recommend that you use the Official API of WhatsApp for Business.

We try to protect you from being permanently blocked in several ways, but WAFVEL is not responsible if the number you are using is permanently blocked by WhatsApp due to Spamming activities, or other things. Some additional protection from us:

    - Check the recipient's cellphone number whether it is a WhatsApp number or not.
    - Rate Limiting API - Free: 1 message per 1 minute. Paid: 10 messages per 1 minute. There are times when you are dev, there are errors accessing the API repeatedly, causing you to send hundreds of messages in 1 second, or your token is used by other people. With the Rate Limiting API, it will minimize blocking from WhatsApp. This rate limiting can change at any time.