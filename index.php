<?php

/**
 * @package Wafvel - WooCommerce New Order Whatsapp Notification
 * @version 0.0.2
 */
/*
Plugin Name: Wafvel - WooCommerce New Order Whatsapp Notification
Plugin URI: https://gitlab.com/randhi.pp/wafvel-woocommerce-new-order-whatsapp-notification
Description: Send Whatsapp Notification every new Order created to admin and customer via wafvel.com API
Author: Randhi Pratama Putra
Version: 0.0.2
License: GPL2
Author URI: https://jombang.digital/
*/

/*
[Wafvel - WooCommerce New Order Whatsapp Notification] is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
 
[Wafvel - WooCommerce New Order Whatsapp Notification] is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with [Wafvel - WooCommerce New Order Whatsapp Notification]. If not, see LICENSE.txt.
*/

function plugin_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=wafvel-options-page">' . __( 'Settings' ) . "</a>";
    array_push( $links, $settings_link );
    return $links;
}
$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'plugin_add_settings_link' );

// add_action( $tag:string, $function_to_add:callable, $priority:integer, $accepted_args:integer )
add_action('woocommerce_new_order', 'wafvel_notif_admin', 30, 1);

function wafvel_notif_admin( $order_id ) {
    if ( ! $order_id )
        return;

    if( ! get_post_meta( $order_id, '_wafvel_send_wa', true ) ) {

        // Get an instance of the WC_Order object
        $order = wc_get_order( $order_id );

        // Get the order key
        $order_key = $order->get_order_key();

        // Get the order number
        $order_number = $order->get_order_number();

        if($order->is_paid())
            $paid = __('yes');
        else
            $paid = __('no');
        
        // Output some data
        $message = "\n\n```ID      ```: [". $order_id ."] ".
                     "\n```Key     ```: " . $order_key. 
                     "\n```Status  ```: " . $order->get_status() . 
                     "\n```is paid ```: " . $paid;

        $message .= "\n\n🛒 *Order Details*\n\n";
        $key = 1;

        // Loop through order items
        foreach ( $order->get_items() as $item_id => $item ) {

            // Get the product object
            $product = $item->get_product();

            // Get the product Id
            $product_id = $product->get_id();

            // Get the product name
            $product_name = $item->get_name();
            
            // Get the product qty
            $product_qty = $item->get_quantity();

            // Get the product price
            $product_price = $product->get_price();

            // Get the product subtotal
            $product_subtotal = $item->get_subtotal();

            // Get the product total
            $product_subtotal = $item->get_total();

            $number = $product_price * $product_qty;
            $message .= "*".$key++ . ". ". $product_name . "*". 
                        "\n``` - Product ID``` : " . $product_id . 
                        "\n``` - Order Qty ``` : " . $product_qty . 
                        "\n``` - Price     ``` : " . get_woocommerce_currency_symbol() ."```".str_pad((number_format($product_price,0,'.',',')), 11, " ", STR_PAD_LEFT)."```".
                        "\n``` - SubTotal  ``` : " . get_woocommerce_currency_symbol() ."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
            if($item->get_total_tax() !== 0){
                $number = $item->get_subtotal_tax();
            $message .= "\n``` - Tax       ``` : ".get_woocommerce_currency_symbol()."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
            }            
        }
        $number = $order->get_total()-$order->get_shipping_total()-$order->get_total_tax();
        $message .= "\n--------------------------------------------------------------------";
        $message .=   "\n```Total    ```: ".get_woocommerce_currency_symbol()."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
        if($order->get_total_tax() !== 0){
            $number = $order->get_total_tax();
            $message .= "\n```Tax      ```: ".get_woocommerce_currency_symbol()."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
        }  
        if($order->get_shipping_total() !== 0){
            $number = $order->get_shipping_total();
            $message .= "\n```Shipping ```: ".get_woocommerce_currency_symbol()."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
        }            
        $number = $order->get_total();
        $message .=     "\n```GT       ```: ".get_woocommerce_currency_symbol()."```".str_pad((number_format($number,0,'.',',')), 11, " ", STR_PAD_LEFT)."```";
        $message .= "\n--------------------------------------------------------------------";

        $message .= "\n\n👤 *Billing Details*\n";
        $message .= "\n```Name    ```: ".$order->get_billing_first_name()." ".$order->get_billing_last_name();
        $message .= "\n```Phone   ```: ".$order->get_billing_phone();
        $message .= "\n```Email   ```: ".$order->get_billing_email();
        $message .= "\n```Address ```: \n".$order->get_billing_address_1()." ".$order->get_billing_address_2().", ".$order->get_billing_city()." ".$order->get_billing_state()." ".$order->get_billing_country()." ".$order->get_billing_postcode();

        $message .= "\n\n🚚 *Shipping Details*\n";
        if(get_post_meta($order_id, '_shipping_address_1', true) !== $order->get_billing_address_1() ){
            
            $message .= "\n```Name    ```: ".$order->get_shipping_first_name()." ".$order->get_shipping_last_name();
            $message .= "\n```Phone   ```: "
                .get_post_meta($order_id, '_shipping_phone', true);
            $message .= "\n```Address ```: "
                .get_post_meta($order_id, '_shipping_address_1', true)." "
                .get_post_meta($order_id, '_shipping_address_2', true)." "
                .get_post_meta($order_id, '_shipping_city', true)."\n"
                .get_post_meta($order_id, '_shipping_state', true)." - "
                .get_post_meta($order_id, '_shipping_country', true)." "
                .get_post_meta($order_id, '_shipping_postcode', true);
            
        }
        $message .= "\n```Send via  ```: ".$order->get_shipping_method();
        $message .= "\n```Cust Note ```: ".$order->get_customer_note();
        
        if ( !function_exists( 'wafvel_send_chat' ) ) {
            return;
        }
        // send to admin 
        wafvel_send_chat('admin',$order,$message);

        // send to customer 
        wafvel_send_chat('customer',$order,$message);

        // Flag the action as done (to avoid repetitions on reload for example)
        $order->update_meta_data( '_wafvel_send_wa', true );
        $order->save();
    }
}

function wafvel_send_chat($recipient, $order, $message)
{
    $url  = 'https://wafvel.com:2096/api/whatsapp/send';
    $token = get_option('wafvel-token');

    if( $recipient == 'admin' ){
        $phone = get_option('wafvel-admin-phone');
        $message = "Hello Admin, New Order Received!".$message;
    }
    if( $recipient == 'customer' ){
        $phone = $order->get_billing_phone();
        $message = "Hello ".$order->get_billing_first_name().", Your Order succesfully received by our admin!".$message;

        // add payment info
        if( $order->get_payment_method() == 'bacs'){
            $message = $message."\n\nTotal pending payment *" 
            . get_woocommerce_currency_symbol() . " " 
            . number_format($order->get_total(),2,'.',',') . "*, you can pay / transfer to this bank account :\n";
            
            $bacs_info = get_option( 'woocommerce_bacs_accounts');
            
            $i = 0;
            if ( $bacs_info ) {
                foreach ( $bacs_info as $account ) {
                    $i++;
                    $account_name   = esc_attr( wp_unslash( $account['account_name'] ) );
                    $bank_name      = esc_attr( wp_unslash( $account['bank_name'] ) );
                    $account_number = esc_attr( $account['account_number'] );
                    $sort_code      = esc_attr( $account['sort_code'] );
                    $iban_code      = esc_attr( $account['iban'] );
                    $bic_code       = esc_attr( $account['bic'] );
                    
                    $message       .= $bank_name.' - '.$account_name."\nNo. : *".$account_number."* \n"; 
                    if(isset($sort_code))
                        $message   .= "```Shortcode ```: ".$sort_code."\n";
                    if(isset($iban_code))
                        $message   .= "```IBAN      ```: ".$iban_code."\n";
                    if(isset($bic_code))
                        $message   .= "```BIC/SWIFT ```: ".$iban_code."\n";
                    $message .= "\n";  
                }
                $message .= "\nTo change payment method, click here : \n".$order->get_checkout_payment_url();

            }
        }

        if( $order->get_payment_method() == 'midtrans'){
            $message = $message."\n\nTotal pending payment *" 
            . get_woocommerce_currency_symbol() . " " 
            . number_format($order->get_total(),2,'.',',') . "* \nYou can pay using Midtrans supported method, card, transfer, internet banking, tcash, gopay,etc. Click this payment url :\n\n"
			. $order->get_checkout_payment_url();
        }
    }

    $json = json_encode([
        "token" => $token,
        "phone" => $phone,
        "message" => $message
    ]);

    wp_remote_post( $url,  array(
        'headers'     => array('Content-Type' => 'application/json; charset=utf-8'),
        'body'        => $json,
        'method'      => 'POST',
        'data_format' => 'body',
    ));
}

function wafvel_send_test_chat()
{
    ?>
	<script type="text/javascript">
	jQuery(document).ready(function($) {

		var data = {
			'token': $('#wafvel-token').val(),
			'phone': $('#wafvel-admin-phone').val(),
            'message': 'Test from wordpress success!!'
		};

        $('#testbutton').click(function(e) {
            e.preventDefault(); // prevent form from reloading page
            
            // setTimeout( function ( ) { alert("Test will start now, if success, you will receive a message to your admin phone number."); }, 3000 );
            
            jQuery.ajax({
                url: 'https://wafvel.com:2096/api/whatsapp/send',
                type: 'POST',
                data: JSON.stringify(data),
                processData: false,
                contentType: 'application/json',
                success: function () {
                    alert("Test OK!");
                },
                error: function (jqxhr) {
                    alert("Test failure, server sent : "+ jqxhr.responseText );
                }
            });
        });
    });
	</script> <?php
}

add_action( 'admin_menu', 'add_wafvel_options_page' );
function add_wafvel_options_page() {

	add_options_page(
		'Wafvel Options',
		'Wafvel Options',
		'manage_options',
		'wafvel-options-page',
		'display_wafvel_options_page'
	);

}

function display_wafvel_options_page() {

    echo '<h2>Wafvel WooCommerce New Order Notification - Options Page</h2>';
    
    include 'include/settingInfo.php';

}

add_action( 'admin_init', 'wafvel_admin_init_one' );
function wafvel_admin_init_one() {

	add_settings_section(
		'wafvel-settings-section-one',      
		'Step 1 : Input Wafvel Token',         
		'display_wafvel_settings_message',  
		'wafvel-options-page'               
	);

	add_settings_field(
		'wafvel-token',        
		'Paste Token Here',        
		'render_wafvel_token',  
		'wafvel-options-page',        
		'wafvel-settings-section-one' 
	);

	register_setting(
		'wafvel-settings',    
		'wafvel-token'    
	);

}

function display_wafvel_settings_message() {
    include 'include/tokenSettingMessage.php';
}

function render_wafvel_token() {

	$input = get_option( 'wafvel-token' );
	echo '<input type="text" id="wafvel-token" name="wafvel-token" value="' . $input . '" />';

}

add_action( 'admin_init', 'wafvel_admin_init_two' );
function wafvel_admin_init_two() {

	add_settings_section(
		'wafvel-settings-section-two',
		'Step 2 : Input Admin Phone',
		'display_another_wafvel_settings_message',
		'wafvel-options-page'
	);

	add_settings_field(
		'wafvel-admin-phone',
		'Admin Phone Number',
		'render_wafvel_input_field_two',
		'wafvel-options-page',
		'wafvel-settings-section-two'
	);

	register_setting(
		'wafvel-settings',
		'wafvel-admin-phone'
	);

}

function display_another_wafvel_settings_message() {
	echo "This admin phone means any phone number (with whatsapp active)<br>which you want to send admin notification message to.";
}

function render_wafvel_input_field_two() {

	$input = get_option( 'wafvel-admin-phone' );
	echo '<input type="text" id="wafvel-admin-phone" name="wafvel-admin-phone" value="' . $input . '" />';

}
